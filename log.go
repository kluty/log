package log

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"runtime"
	"strings"
	"time"
)

// Body with size over RequestResponseBodyLimit will be logged partially - only bytes under limit.
var RequestResponseBodyLimit = 3 * 1 << 10

// Customize Writer for project in init function.
// For example:
//   log.Writer = ioutil.Discard
// NOTE: Writer must be concurrently safe.
var Writer io.Writer = stdout{}

// HOSTNAME is set only in bash and is not present in environment variables (check by env command).
// That's why os.Getenv("HOSTNAME") returns empty string.
var HOSTNAME, _ = os.Hostname()

var ENVIRONMENT = ""

func Init(environment string, writer io.Writer, bodyLimit int) {
	ENVIRONMENT = environment
	if writer != nil {
		Writer = writer
	}
	if bodyLimit != 0 {
		RequestResponseBodyLimit = bodyLimit
	}
}

type Event struct {
	// Short description of event. Details are provided in other fields.
	// Examples:
	//
	//  1. Error is returned by a function call.
	//
	//     "failed db.CreateArticle"
	//
	//     Use 'failed', followed by a name of a function call.
	//     No need to use preposition 'to'. No need to use synonyms for a function name.
	//
	//  2. Job feedback.
	//     It's important to give feedback on job completion. Because otherwise it's not clear if job was done at all.
	//
	//     "completed track-job"
	//
	//     Use 'completed', followed by a name of a job.
	//
	//  3. HTTP transaction (transaction is request and response to this request).
	//     In case of failed transaction, client may reference it in order to understand what went wrong.
	//     In order to solve such issues it's important to log request and response.
	//     Especially when request handling run such operations as:
	//       - insert/update/delete record in database
	//       - validations/calculations
	//     To make logs more readable it's better to have both request and response in one logs message.
	//     Handlers can not only receive requests, they often send them. That's why it's better to
	//     clarify direction of transaction.
	//
	//     "in 'POST localhost:8080/api/v1/article' 201"
	//     "out 'GET example.org' 200"
	//
	//     Use 'in' and 'out' to indicate direction of transaction.
	//       in - handler received this request and responds to it with this response.
	//       out - handler sent this request and in return got this response.
	//     Do not write path query, as it might increase message length and clutter it with unnecessary details.
	Message string `json:"message"`

	// Should be set by logs sender, not by logs receiver.
	// Format: RFC3339, UTC timezone.
	Timestamp string `json:"timestamp"`

	// Use cases:
	//  - to track connected chain of events,
	//  - user can reference specific request, as reference id is supposed to be sent to user in case of errors.
	ReferenceID string `json:"reference_id,omitempty"`

	// Most of the requests are not made anonymously.
	// To improve readability of request and speed of debug,
	// because there will be no need to identify user of request by e.g. token in headers or request body.
	User_ string `json:"user,omitempty"`

	// Short name of the error, without details.
	// Used by notifiers: if error exists then notifier will send notification.
	// Type is string, because structure errorString is not serializable (there are no public fields).
	Error string `json:"error,omitempty"`

	// String representation of function arguments, constants...
	// Type of value should be identified from the code where event happened.
	// No need to save numbers as integers:
	//   - log database is unlikely to be used to calculate values.
	// No need to serialize structures as JSON:
	//   - many structures are not intended to be represented as JSON,
	//   - it's unlikely to query log database by structure field.
	// For structure serializing prefer "%#v". NOTE: values of reference type are not readable!
	Context map[string]string `json:"context,omitempty"`

	Request  *request  `json:"request,omitempty"`
	Response *response `json:"response,omitempty"`

	// In kubernetes HOSTNAME is equal to pod's name.
	// In docker-compose use 'hostname' param to set HOSTNAME  inside container.
	Hostname string `json:"hostname,omitempty"`

	// production or staging
	Environment string `json:"environment,omitempty"`

	// There are many functions with the same name.
	// Message with a name of just a function is not enough to find a place of error in code.
	// That is why we need to provide additional information about the caller, that initiated logging.
	Caller *caller `json:"caller,omitempty"`
}

func newEvent(m string) *Event {
	return &Event{
		Message:     m,
		Timestamp:   time.Now().UTC().Format(time.RFC3339Nano),
		Hostname:    HOSTNAME,
		Environment: ENVIRONMENT,
	}
}

func Failed(fn string) *Event {
	e := newEvent("failed " + fn)
	setCaller(e, 2)
	return e
}

func Completed(fn string) *Event {
	return newEvent("completed " + fn)
}

func Msg(m string) *Event {
	return newEvent(m)
}

func (e *Event) String() string {
	return string(marshal(e))
}

func (e *Event) Bytes() []byte {
	return marshal(e)
}

func (e *Event) Println() {
	fmt.Println(e)
}

// Clients are not expected to handle logging errors. All Write errors will be written to stdout.
func (e *Event) Write() {
	b := e.Bytes()
	if b != nil {
		if _, err := Writer.Write(b); err != nil {
			Failed("Writer.Write").
				RefID(e.ReferenceID).
				Err(err).
				Ctx(map[string]string{"data": fmt.Sprintf("%q", b)}).
				Println()
		}
	}
}

func (e *Event) RefID(r string) *Event {
	e.ReferenceID = r
	return e
}

func (e *Event) User(u string) *Event {
	e.User_ = u
	return e
}

func (e *Event) Err(err error) *Event {
	e.Error = err.Error()
	setCaller(e, 2)
	return e
}

func (e *Event) Ctx(c map[string]string) *Event {
	e.Context = c
	return e
}

// http.Request is not required as argument, because it's client responsibility to read request body and clone body for further use.
// http.Request is complex structure with many fields, only part of which are used in logging.
// Argument list explicitly specifies only required data.
func (e *Event) Req(method, host, path string, query url.Values, headers http.Header, body []byte) *Event {
	if method == "" && host == "" && path == "" && len(query) == 0 && len(headers) == 0 && len(body) == 0 {
		return e
	}
	b, br := formatBody(body)
	e.Request = &request{
		Method:  method,
		Host:    host,
		Path:    path,
		Query:   query,
		Headers: formatHeaders(headers),
		Body:    b,
		BodyRaw: br,
	}
	return e
}

func (e *Event) Resp(statusCode int, headers http.Header, body []byte) *Event {
	if statusCode == 0 && len(headers) == 0 && len(body) == 0 {
		return e
	}
	b, br := formatBody(body)
	e.Response = &response{
		StatusCode: statusCode,
		Headers:    formatHeaders(headers),
		Body:       b,
		BodyRaw:    br,
	}
	return e
}

type caller struct {
	File string `json:"file,omitempty"`
	Line int    `json:"line,omitempty"`
}

func setCaller(e *Event, skip int) {
	if e.Caller != nil {
		return
	}
	_, file, line, _ := runtime.Caller(skip)
	e.Caller = &caller{
		File: file,
		Line: line,
	}
}

type request struct {
	Method string     `json:"method,omitempty"`
	Host   string     `json:"host,omitempty"`
	Path   string     `json:"path,omitempty"`
	Query  url.Values `json:"query,omitempty"`

	// Multiple header values are joined by comma.
	Headers map[string]string `json:"headers,omitempty"`

	// We need both Body and BodyRaw fields. Only one of them is populated.
	// Body is a JSON representation of request body. BodyRaw is a string representation.
	//
	// If it's possible to unmarshal body to map then it is assigned to Event as a map.
	// The map will be marshalled as JSON.
	// And this enables us to query body fields in document database.
	//
	// If it's not possible to unmarshal, then body is assigned to Event as a string.
	// The string with double quotes will be marshalled with escaped double quotes.
	//
	// If we use here only one field for body, e.g. of a string type, then string representation of JSON will contain double quotes.
	// These double quotes after marshaling of the whole Event will be escaped.
	// And such string will not be recognized as JSON in document database, which will prevent us from querying of body fields.
	Body    map[string]interface{} `json:"body,omitempty"`
	BodyRaw string                 `json:"body_raw,omitempty"`
}

type response struct {
	StatusCode int `json:"status_code"`

	// Multiple header values are joined by comma.
	Headers map[string]string `json:"headers,omitempty"`

	Body    map[string]interface{} `json:"body,omitempty"`
	BodyRaw string                 `json:"body_raw,omitempty"`
}

// Marshal errors are not returned, because any error is unlikely.
// There are two possible errors:
//   1 Type error - impossible here, because we do not pass chan or function to Event constructors or field setters.
//   2 Value error - impossible here, because we always pass to Marshal only structure.
// But nevertheless we handle error returned from Marshal by printing it to stdout.
func marshal(e *Event) []byte {
	log, err := json.Marshal(&e)
	if err != nil {
		Failed("json.Marshal").
			RefID(e.ReferenceID).
			Err(err).
			Ctx(map[string]string{"event": fmt.Sprintf("%#v", e)}).
			Println()
		return nil
	}
	return log
}

func formatHeaders(headers http.Header) map[string]string {
	if len(headers) == 0 {
		return nil
	}
	h := make(map[string]string)
	for header, values := range headers {
		h[header] = strings.Join(values, ", ")
	}
	return h
}

func formatBody(body []byte) (map[string]interface{}, string) {
	if len(body) == 0 {
		return nil, ""
	}
	if len(body) > RequestResponseBodyLimit {
		return nil, string(body[:RequestResponseBodyLimit]) + " [the rest is cut]"
	}
	b := make(map[string]interface{})
	err := json.Unmarshal(body, &b)
	if err != nil {
		return nil, string(body)
	}
	return b, ""
}

type stdout struct{}

// Newline is appended, otherwise all logs will be written as one line.
func (w stdout) Write(p []byte) (n int, err error) {
	n, err = os.Stdout.Write(p)
	if err != nil {
		return
	}
	os.Stdout.Write([]byte("\n"))
	return
}

type HttpWriter struct {
	URL string
}

func (w HttpWriter) Write(p []byte) (n int, err error) {
	r, err := http.NewRequest("POST", w.URL, bytes.NewReader(p))
	if err != nil {
		Failed("http.NewRequest").Err(err).Ctx(map[string]string{"body": fmt.Sprint(p)}).Println()
		return 0, err
	}
	r.Header.Set("Content-Type", "application/json")
	c := http.Client{Timeout: 5 * time.Second}

	for tries := 0; tries < 3; tries++ {
		_, err = c.Do(r)
		if err == nil {
			return len(p), nil
		}
	}
	return
}
